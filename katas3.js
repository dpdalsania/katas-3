// One Through Twenty
function oneThroughTwenty () {
    const div = document.createElement("div");
    for (let counter = 1; counter <= 20; counter++) {
        div.innerHTML += counter + ' ';
    }
    const currentDiv = document.getElementById("div1");
    currentDiv.appendChild(div);
}
oneThroughTwenty()

//Even number
function evensToTwenty () {
    const div = document.createElement("div");
    for (let counter = 2; counter <= 20; counter++) {
        if (counter % 2 === 0) {
            div.innerHTML += counter + ' ';
        }
        const currentDiv = document.getElementById("div2");
        currentDiv.appendChild(div);
    }
}
evensToTwenty()

//Odd number
function oddsToTwenty () {
    const div = document.createElement("div");
    for (let counter = 1; counter <= 20; counter++) {
        if (counter % 2 === 1) {
            div.innerHTML += counter + ' ';
        }
        const currentDiv = document.getElementById("div3");
        currentDiv.appendChild(div);
    }
}
oddsToTwenty()

//Multiples of five
function multiplesOfFive () {
    const div = document.createElement("div");
    for (let counter = 1; counter <= 100; counter++) {
        if (counter % 5 === 0) {
            div.innerHTML += counter + ' ';
        }
    }
    const currentDiv = document.getElementById("div4");
    currentDiv.appendChild(div);
}
multiplesOfFive()

//Square Number
function squareNumbers () {
    const div = document.createElement("div");
    const numbers = []
    for (let counter = 1; counter <= 10; counter += 1) {
        numbers.push(counter ** 2)
        div.innerHTML = numbers.join(" ");
    }
    const currentDiv = document.getElementById("div5");
    currentDiv.appendChild(div);
}
squareNumbers()

//CountingBackwards
function countingBackwards () {
    const div = document.createElement("div");
    const numbers = []
    for (let counter = 20; counter >= 1; counter--) {
        numbers.push(counter)
        div.innerHTML += counter + ' ';
    }
    const currentDiv = document.getElementById("div6");
    currentDiv.appendChild(div);
}
countingBackwards()

//Even Backwards
function evenNumbersBackwards () {
    const div = document.createElement("div");
    const numbers = []
    for (let counter = 20; counter >= 2; counter -= 2) {
        numbers.push(counter)
        div.innerHTML += counter + ' ';
    }
    const currentDiv = document.getElementById("div7");
    currentDiv.appendChild(div);
}
evenNumbersBackwards()

//Odd Backwards
function oddNumbersBackwards () {
    const div = document.createElement("div");
    const numbers = []
    for (let counter = 20; counter >= 1; counter -= 1) {
        if (counter % 2 === 1) {
            numbers.push(counter)
            div.innerHTML += counter + ' ';
        }
        //currentDiv.appendChild(div);
    }
    const currentDiv = document.getElementById("div8");
    currentDiv.appendChild(div);
}
oddNumbersBackwards()

//Multiples Backwards
function multiplesOfFiveBackwards () {
    const div = document.createElement("div");
    const numbers = []
    for (let counter = 100; counter >= 1; counter -= 1) {
        if (counter % 5 === 0) {
            numbers.push(counter)
            div.innerHTML += counter + ' ';
        }
        const currentDiv = document.getElementById("div9");
        currentDiv.appendChild(div);
    }
}
multiplesOfFiveBackwards()

//Square Backwards
function squareNumbersBackwards () {
    const div = document.createElement("div");
    const numbers = []
    for (let counter = 10; counter >= 1; counter -= 1) {
        numbers.push(counter ** 2)
        div.innerHTML = numbers.join(" ");
    }
    const currentDiv = document.getElementById("div10");
    currentDiv.appendChild(div);
}
squareNumbersBackwards()

//20 elements of sampleArray

function sampleArray () {
    const div = document.createElement("div");
    let numbers = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    let value = numbers.toString();
    document.getElementById.innerHTML = value;
    for (let i = 0; i < numbers.length; i++) {
        div.innerHTML += numbers[i] + ' ';
    }
    const currentDiv = document.getElementById("div11");
    currentDiv.appendChild(div);
}
sampleArray()

//Even number from sampleArray

function eventoArray () {
    const div = document.createElement("div");
    let numbers = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    let value = numbers.toString();
    document.getElementById.innerHTML = value;
    for (let i = 1; i <= numbers.length; i++) {
        if (numbers[i] % 2 === 0) {
            div.innerHTML += numbers[i] + ' ';
        }
        const currentDiv = document.getElementById("div12");
        currentDiv.appendChild(div);
    }
}
eventoArray()

//Odd number from sampleArray

function oddtoArray () {
    const div = document.createElement("div");
    let numbers = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    let value = numbers.toString();
    document.getElementById.innerHTML = value;
    for (let i = 0; i <= numbers.length; i++) {
        if (numbers[i] % 2 === 1) {
            div.innerHTML = div.innerHTML + numbers[i] + ' ';
        }
        const currentDiv = document.getElementById("div13");
        currentDiv.appendChild(div);
    }
}
oddtoArray()

//Square number from sampleArray

function squaretoArray () {
    const div = document.createElement("div");
    let numbers = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    for (let i = 1; i < numbers.length; i++) {
        div.innerHTML += numbers[i] * numbers[i] + ' ';
    }
    const currentDiv = document.getElementById("div14");
    currentDiv.appendChild(div);
}

squaretoArray()

//Sum of 1 to 20

function sumofNumber () {
    const div = document.createElement("div");
    let sum = 0;
    for (let i = 1; i <= 20; i++) {
        sum += i;
    }
    div.innerHTML += sum;
    const currentDiv = document.getElementById("div15");
    currentDiv.appendChild(div);
    return sum;
}
sumofNumber()

//Sum of sampleArray

function sumofArray () {
    const div = document.createElement("div");
    let sum = 0;
    let numbers = [469, 755, 244, 245, 758, 450, 302, 20, 71, 2, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    for (let i = 0; i < numbers.length; i++) {
        sum += numbers[i];
    }
    div.innerHTML += sum;
    const currentDiv = document.getElementById("div16");
    currentDiv.appendChild(div);
    return sum;
}
sumofArray()

//Smallest element from Array

function smallest () {
    const div = document.createElement("div");
    const numbers = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    let smallest = numbers[0];
    for (let i = 0; i < numbers.length; i++) {
        if (numbers[i] < smallest) {
            smallest = numbers[i];
        }
        div.innerHTML = smallest;
    }
    const currentDiv = document.getElementById("div17");
    currentDiv.appendChild(div);
}
smallest()

//Largest element from Array
function largestnum () {
    const div = document.createElement("div");
    const numbers = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    let largest = numbers[0];
    for (let i = 0; i < numbers.length; i++) {
        if (numbers[i] > largest) {
            largest = numbers[i];
        }
        div.innerHTML = largest;
    }
    const currentDiv = document.getElementById("div18");
    currentDiv.appendChild(div);
    return largest;

}
largestnum()

// Display 20 box
function box () {
    const parentDiv = document.getElementById("div19");
    for (let i = 0; i < 20; i++) {
        const childDiv = document.createElement("div");
        childDiv.style.width = '100px';
        childDiv.style.height = '20px';
        childDiv.style.backgroundColor = 'grey';
        childDiv.style.margin = '1px';
        parentDiv.appendChild(childDiv);
    }

}
box()

//20 solid gray rectangles, each 20px high, with widths ranging evenly from 105px to 200px 

function rectangles () {
    const parentDiv = document.getElementById("div20");
    for (let i = 105; i <= 200; i = i + 5) {
        const childDiv = document.createElement("div");
        childDiv.style.width = i + "px";
        childDiv.style.height = 20 + "px";
        childDiv.style.backgroundColor = 'grey';
        childDiv.style.margin = '10px';
        parentDiv.appendChild(childDiv);
    }

}
rectangles()

//20 solid gray rectangles, each 20px high, with widths in pixels given by the 20 elements of sampleArray.

function element () {
    const parentDiv = document.getElementById("div21");
    let sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 71, 2, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    for (let i = 0; i <= sampleArray.length; i++) {
        const childDiv = document.createElement("div");
        childDiv.style.width = sampleArray[i] + "px";
        childDiv.style.height = 20 + "px";
        childDiv.style.backgroundColor = 'grey';
        childDiv.style.margin = '10px';
        parentDiv.appendChild(childDiv);
    }

}
element()

//alternate colors so that every other rectangle is red.

function color () {
    const parentDiv = document.getElementById("div22");
    let sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 71, 2, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    for (let i = 0; i <= sampleArray.length; i++) {
        const childDiv = document.createElement("div");
        childDiv.style.width = sampleArray[i] + "px";
        childDiv.style.height = 20 + "px";

        if (i % 2 == 0) {
            childDiv.style.backgroundColor = 'red';
        }
        else {
            childDiv.style.backgroundColor = 'grey';
        }
        childDiv.style.margin = '10px';
        parentDiv.appendChild(childDiv);
    }
}
color()

//color the rectangles with even widths red.

function even () {
    const parentDiv = document.getElementById("div23");
    let sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 71, 2, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    for (let i = 0; i <= sampleArray.length; i++) {
        const childDiv = document.createElement("div");
        childDiv.style.width = sampleArray[i] + "px";
        childDiv.style.height = 20 + "px";

        if (sampleArray[i] % 2 == 0) {
            childDiv.style.backgroundColor = 'red';
        }
        else {
            childDiv.style.backgroundColor = 'grey';
        }
        childDiv.style.margin = '10px';
        parentDiv.appendChild(childDiv);
    }
}
even()

